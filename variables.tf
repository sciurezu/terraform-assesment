variable "azure-subscription-id" {
  type        = string
  description = "Azure Subscription ID"
}
variable "azure-client-id" {
  type        = string
  description = "Azure Client ID"
}
variable "azure-client-secret" {
  type        = string
  description = "Azure Client Secret"
}
variable "azure-tenant-id" {
  type        = string
  description = "Azure Tenant ID"
}
variable "location" {
  type        = string
  description = "Azure region for the resource group"
  default     = "West Europe"
}
variable "vm_number" {
  type        = number
  description = "Number of vms to create"
  default     = "2"
}
variable "linux_admin_username" {
  type        = string
  description = "Username for Virtual Machine administrator account"
  default     = "assesment_admin"
}
variable "linux_admin_password" {
  type        = string
  description = "Password for Virtual Machine administrator account"
  default     = ""
}
variable "linux_vm_image_publisher" {
  type        = list(any)
  description = "publisher of the image used to create the virtual machine. ex: Canonical(Ubuntu), RedHat, OpenLogic(CentOS)"
  default     = ["Canonical", "Canonical"]
}
variable "linux_vm_image_offer" {
  type        = list(any)
  description = "the image used to create the virtual machine. ex: UbuntuServer, RHEL, CentOS"
  default     = ["UbuntuServer", "UbuntuServer"]
}
variable "linux_vm_image_sku" {
  type        = list(any)
  description = "Image version for the deployment. ex: 16.04-LTS, 7-RAW, 7.5"
  default     = ["18.04-LTS", "18.04-LTS"]
}