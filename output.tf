output "resource_group_name" {
  value = azurerm_resource_group.assesment-rg.name
}
output "vm_public_ip_list" {
  value     = azurerm_public_ip.linux-vm-ip.*.ip_address
  sensitive = true
}
output "vm_private_ip_list" {
  value     = azurerm_network_interface.linux-vm-nic.*.private_ip_address
  sensitive = true
}