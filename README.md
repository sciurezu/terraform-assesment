# Terraform Assesment

Terraform code used to create a configurable number of VMs (any number between 2
and 100) in Azure 
For each VM the following parameters can be specified: VM flavor and VM image


Configurable variables in terraform.tfvars

- linux_admin_username     -> The admin username for the VMs
- location                 -> Location where the VMs are created in Azure
- vm_number                -> Number of vms to be deployed (default value: 2)
- linux_vm_image_publisher -> publisher of the image used to create the virtual machine. ex: Canonical(Ubuntu), RedHat, OpenLogic(CentOS) (default value: ["Canonical", "Canonical"] )
- linux_vm_image_offer     -> the image used to create the virtual machine. ex: UbuntuServer, RHEL, CentOS (default value: ["UbuntuServer", "UbuntuServer"])
- linux_vm_image_sku       -> Image version for the deployment. ex: 16.04-LTS, 7-RAW, 7.5 (default value: ["18.04-LTS", "18.04-LTS"])


Before running the code, you need to create a 'Contributor' role assignment in azure
- **az ad sp create-for-rbac --role="Contributor" --scopes="/subscriptions/<azure-subscription-id>"**

After that you need to add the values for:
- > azure-subscription-id = xxxx
- > azure-client-id       = xxxx
- > azure-client-secret   = xxxx
- > azure-tenant-id       = xxxx


To execute the code, please run the following commands:
- terraform init
- terraform plan -out=tfplan
- terraform apply tfplan  


