azure-subscription-id = "xxxxx"
azure-client-id       = "xxxxx"
azure-client-secret   = "xxxxx"
azure-tenant-id       = "xxxxx"
linux_admin_username  = "assesment_admin"
location              = "West Europe"

#Number of vms to be deployed
#vm_number = "3"

#publisher of the image used to create the virtual machine. ex: Canonical(Ubuntu), RedHat, OpenLogic(CentOS)
#linux_vm_image_publisher = ["Canonical", "Canonical", "OpenLogic"]

#the image used to create the virtual machine. ex: UbuntuServer, RHEL, CentOS
#linux_vm_image_offer = ["UbuntuServer", "UbuntuServer", "CentOS"]

#Image version for the deployment. ex: 16.04-LTS, 7-RAW, 7.5
#linux_vm_image_sku = ["16.04-LTS", "18.04-LTS", "7.5"]
