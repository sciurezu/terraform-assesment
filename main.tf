terraform {

  required_version = ">=0.12"

  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>2.0"
    }
  }
}

provider "azurerm" {
  features {}
  environment     = "public"
  subscription_id = var.azure-subscription-id
  client_id       = var.azure-client-id
  client_secret   = var.azure-client-secret
  tenant_id       = var.azure-tenant-id
}

resource "azurerm_resource_group" "assesment-rg" {
  name     = "assesment-rg"
  location = var.location
}
# Create the network VNET
resource "azurerm_virtual_network" "assesment-vnet" {
  name                = "assesment-vnet"
  address_space       = ["10.0.0.0/16"]
  resource_group_name = azurerm_resource_group.assesment-rg.name
  location            = azurerm_resource_group.assesment-rg.location
}
# Create a subnet for VM
resource "azurerm_subnet" "vm-subnet" {
  name                 = "vm-subnet"
  address_prefixes     = ["10.0.2.0/24"]
  virtual_network_name = azurerm_virtual_network.assesment-vnet.name
  resource_group_name  = azurerm_resource_group.assesment-rg.name
}

# Create Security Group to access linux
resource "azurerm_network_security_group" "linux-vm-nsg" {
  depends_on          = [azurerm_resource_group.assesment-rg]
  name                = "linux-vm-nsg"
  location            = azurerm_resource_group.assesment-rg.location
  resource_group_name = azurerm_resource_group.assesment-rg.name

  security_rule {
    name                       = "AllowSSH"
    description                = "Allow SSH"
    priority                   = 150
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "Internet"
    destination_address_prefix = "*"
  }
}

# Associate the linux NSG with the subnet
resource "azurerm_subnet_network_security_group_association" "linux-vm-nsg-association" {
  depends_on                = [azurerm_resource_group.assesment-rg]
  subnet_id                 = azurerm_subnet.vm-subnet.id
  network_security_group_id = azurerm_network_security_group.linux-vm-nsg.id
}
resource "azurerm_public_ip" "linux-vm-ip" {
  depends_on          = [azurerm_resource_group.assesment-rg]
  count               = var.vm_number
  name                = "linux-vm-ip.${count.index}"
  location            = azurerm_resource_group.assesment-rg.location
  resource_group_name = azurerm_resource_group.assesment-rg.name
  allocation_method   = "Static"
}
# Create Network Card for linux VM
resource "azurerm_network_interface" "linux-vm-nic" {
  depends_on          = [azurerm_resource_group.assesment-rg]
  count               = var.vm_number
  name                = "linux-vm-nic.${count.index}"
  location            = azurerm_resource_group.assesment-rg.location
  resource_group_name = azurerm_resource_group.assesment-rg.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.vm-subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = element(azurerm_public_ip.linux-vm-ip.*.id, count.index)
  }
}

# Generate random password
resource "random_password" "linux-vm-password" {
  count            = var.vm_number
  length           = 16
  min_upper        = 2
  min_lower        = 2
  min_special      = 2
  numeric          = true
  special          = true
  override_special = "!@#$%&"
}

resource "azurerm_virtual_machine" "assesment_vm_generate" {
  count                 = var.vm_number
  name                  = "assesment_vm_${count.index}"
  location              = azurerm_resource_group.assesment-rg.location
  resource_group_name   = azurerm_resource_group.assesment-rg.name
  network_interface_ids = [element(azurerm_network_interface.linux-vm-nic.*.id, count.index)]
  vm_size               = "Standard_DS1_v2"

  storage_image_reference {
    publisher = var.linux_vm_image_publisher[count.index]
    offer     = var.linux_vm_image_offer[count.index]
    sku       = var.linux_vm_image_sku[count.index]
    version   = "latest"
  }
  storage_os_disk {
    name              = "myosdisk-${count.index}"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
    computer_name  = "hostname${count.index}"
    admin_username = var.linux_admin_username
    admin_password = random_password.linux-vm-password[count.index].result
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }
}
resource "null_resource" "test_ping_connection" {
  count      = var.vm_number
  depends_on = [azurerm_virtual_machine.assesment_vm_generate]
  provisioner "local-exec" {
    command = "sleep 60 && sshpass -p '${random_password.linux-vm-password[count.index].result}' ssh -o StrictHostKeyChecking=no ${var.linux_admin_username}@${element(azurerm_public_ip.linux-vm-ip.*.ip_address, count.index)} 'hostname -I && ping -c4 ${element(azurerm_network_interface.linux-vm-nic.*.private_ip_address, count.index + 1)}'  >> ping_ip.txt"
  }
}